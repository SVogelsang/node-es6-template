const DeleteMe = require('../src/delete-me');

describe('a demo test', () => {

  it('should succeed', () => {
    const sut = new DeleteMe('my name');
    expect(sut.getName()).to.equal('my name');
  })
});