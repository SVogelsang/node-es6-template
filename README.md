# Node ES6 Template

This is a simple template for setting up a Node project that supports ES6.

## Getting Started

### Prerequisites

- [Node.js and npm](nodejs.org) Node ^6.10.0, npm ^3.10.10
- [yarn](https://github.com/yarnpkg/yarn) ```npm install -g yarn```

### Developing

1. Run `yarn install` to install dependencies.

## Build, Test and Development

1. Run `yarn start` for execution and change tracking.
2. Run `yarn run build` for building.
3. Run `yarn test` for testing.
4. Run `yarn run report` for code coverage and test results.

## Testing

Tests are executed using [Mocha](https://mochajs.org/). All files that ends with `.spec.js` located in `tests` directory are executed. 

## Structure

```
├── src
|   └── <the sources>
├── tests
    ├── mocha.conf.js
    └── <tests or directories containing tests>
```
mocha.conf.js contains global configuration for macha (chai, sinon).

## Versioning

When planning a new versions:
1. Keep your [CHANGELOG.md](CHANGELOG.md) up to date in the *Unreleased* section
2. Update version and CHANGELOG:
  * Run `npm version patch -m "commit comment"` for a patch/build (e.g. 1.0.12 -> 1.0.13).
  * Run `npm version minor -m "commit comment"` for a minor version increase (e.g. 1.0.12 -> 1.1.0).
  * Run `npm version major -m "commit comment"` for a major version increase (e.g. 1.1.12 -> 2.0.0).
3. Push your changes.

This updates `CHANGELOG.md`, adds release dates and inserts new `Unreleased` section in changelog where new changelog comments should be added for new features etc.
Also the update creates a git tag for the new version. Use `git push origin v0.0.x` or `git push --tag` to push the new tag.

## Report

Code coverage uses [istanbul](https://istanbul.js.org/docs/tutorials/mocha/) to create coverage reports. The current setting uses `html` reporter to write the report to the `coverage` directory. Go to `nyc` section in `package.json` to change the settings.
During coverage the test results are written to `test-results` directory using Mochas XUnit reporter.
These results can be included to the artifacts of a e.g. [Travis-CI](https://travis-ci.org/) build.

 
