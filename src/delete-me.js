
module.exports = class DeleteMe {

  constructor(name){
    this._name = name;
  }

  getName() {
    return this._name;
  }
};
